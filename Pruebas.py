'''
Created on 18 dic. 2020

@author: Ruby
'''

'''Dijkstra'''

import sys 
   
class Graph(): 
   
    def __init__(self, vertices): 
        self.V = vertices 
        self.graph = [[0 for column in range(vertices)]  
                    for row in range(vertices)] 
   
    def printSolution(self, dist): 
        print ("Vertex tDistance from Source") 
        for node in range(self.V): 
            print (node, "t", dist[node]) 
    
    def minDistance(self, dist, sptSet): 

        min = sys.maxsize 
   
        for v in range(self.V): 
            if dist[v] < min and sptSet[v] == False: 
                min = dist[v] 
                min_index = v 
   
        return min_index 
   

    def dijkstra(self, src): 
   
        dist = [sys.maxsize] * self.V 
        dist[src] = 0
        sptSet = [False] * self.V 
   
        for cout in range(self.V): 
   
            u = self.minDistance(dist, sptSet) 
   
            sptSet[u] = True
   

            for v in range(self.V): 
                if self.graph[u][v] > 0 and \
                    sptSet[v] == False and \
                    dist[v] > dist[u] + self.graph[u][v]: 
                    dist[v] = dist[u] + self.graph[u][v] 
   
        self.printSolution(dist) 
   
g = Graph(9) 
g.graph = [[0, 4, 0, 0, 0, 0, 0, 8, 0], 
        [4, 0, 8, 0, 0, 0, 0, 11, 0], 
        [0, 8, 0, 7, 0, 4, 0, 0, 2], 
        [0, 0, 7, 0, 9, 14, 0, 0, 0], 
        [0, 0, 0, 9, 0, 10, 0, 0, 0], 
        [0, 0, 4, 14, 10, 0, 2, 0, 0], 
        [0, 0, 0, 0, 0, 2, 0, 1, 6], 
        [8, 11, 0, 0, 0, 0, 1, 0, 7], 
        [0, 0, 2, 0, 0, 0, 6, 7, 0] 
        ]; 
   

  
# This code is contributed by Divyanshu Mehta 



'''BellmanFord'''
 
class Graph1:  
  
    def __init__(self, vertices):  
        self.V = vertices 
        self.graph = []  
   
    def addEdge(self, u, v, w):  
        self.graph.append([u, v, w])  
          
    def printArr(self, dist):  
        print("Vertex Distance from Source")  
        for i in range(self.V):  
            print("{0}\t\t{1}".format(i, dist[i]))  
      
    
    def BellmanFord(self, src):  
  
        dist = [float("Inf")] * self.V  
        dist[src] = 0
  
        for _ in range(self.V - 1):  
            for u, v, w in self.graph:  
                if dist[u] != float("Inf") and dist[u] + w < dist[v]:  
                        dist[v] = dist[u] + w  
  
        for u, v, w in self.graph:  
                if dist[u] != float("Inf") and dist[u] + w < dist[v]:  
                        print("Graph contains negative weight cycle") 
                        return 
        self.printArr(dist)  
  
g1 = Graph1(5)  
g1.addEdge(0, 1, -1)  
g1.addEdge(0, 2, 4)  
g1.addEdge(1, 2, 3)  
g1.addEdge(1, 3, 2)  
g1.addEdge(1, 4, 2)  
g1.addEdge(3, 2, 5)  
g1.addEdge(3, 1, 1)  
g1.addEdge(4, 3, -3)  
  
# Print the solution  
  
  
# Initially, Contributed by Neelam Yadav  
# Later On, Edited by Himanshu Garg 




'''Floyd Warshall'''

nV = 4

INF = 999


# Algorithm implementation
def floyd_warshall(G):
    distance = list(map(lambda i: list(map(lambda j: j, i)), G))

    # Adding vertices individually
    for k in range(nV):
        for i in range(nV):
            for j in range(nV):
                distance[i][j] = min(distance[i][j], distance[i][k] + distance[k][j])
    print_solution(distance)


# Printing the solution
def print_solution(distance):
    for i in range(nV):
        for j in range(nV):
            if(distance[i][j] == INF):
                print("INF", end=" ")
            else:
                print(distance[i][j], end="  ")
        print(" ")


G = [[0, 3, INF, 5],
         [2, 0, INF, 4],
         [INF, 1, 0, INF],
         [INF, INF, 2, 0]]

# This code is contributed by Nikhil Kumar Singh(nickzuck_007) 

op=0

while op!=4:
    print("1. Dijkstra")
    print("2. BellmanFord")
    print("3. Floyd Warshall")
    print("4. Salir")
    op=int(input("Elige una opcion"))
    
    if op<1 or op>4:
        print("Opcion invalida")
    elif op==1:
        g.dijkstra(0)
        print()
    elif op==2:
        g1.BellmanFord(0)
        print()
    elif op==3:
        floyd_warshall(G)
        print()
    elif op==4:
        break
    

